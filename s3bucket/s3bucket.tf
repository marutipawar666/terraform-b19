provider "aws" {
    region = "ap-southeast-2"
}

resource "aws_s3_bucket" "s3_bucket" {
    bucket = "firstmrt"
    acl = "private"
    tags = {
        tag = "my bucket"
    }
}

resource "aws_iam_user" {
    name = "tejash"
    path = "/"
}

resource "aws_iam_policy" "bucket_policy" {
    name = "bucket_policy_s3"
    description = "this is for demo "

    policy = jsonencode({
        Version = "2012-10-17",
        Statement = [
        {
            Action = "s3:PutBucketPolicy",
            Effect = "Allow"
            Resource = "arn:aws:s3":::firstmrt"
        }
        ]
    })
}
resource "aws_iam_policy_attachment" "attachment" {
    name = "policy_attachment"
    users = [aws_iam_user.iam_user.name]
    policy_arn = aws_iam_policy.bucket_policy.arn
    

}
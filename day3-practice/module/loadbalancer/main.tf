resource "aws_lb_target_group" "tg_home" {
  name     = "${var.project}-tg-home"
  port     = 80
  protocol = "HTTP"
  vpc_id = var.vpc_id
  tags = {
    Env = var.env
  }
  health_check {
    path = "/"
  }
}

resource "aws_lb_target_group" "tg_digital" {
  name     = "${var.project}-tg-digital"
  port     = 80
  protocol = "HTTP"
  vpc_id = var.vpc_id
  tags = {
    Env = var.env
  }
  health_check {
    path = "/digital/"
  }
}

resource "aws_lb_target_group" "tg_clouth" {
  name     = "${var.project}-tg-clouth"
  port     = 80
  protocol = "HTTP"  
  vpc_id = var.vpc_id
  tags = {
    Env = var.env
  }
  health_check {
    path = "/clouth/"
  }
}

resource "aws_lb" "lb" {
  name               = "${var.project}-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.security_group_id]
  subnets            = var.subnet_ids
  tags = {
    Env = var.env
  }
}

resource "aws_lb_listener" "home" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg_home.arn
  }
}

resource  "aws_lb_listener_rule" "clouth_rule" {
  listener_arn = aws_lb_listener.home.arn
  priority     = 101

  condition {
    path_pattern {
      values = ["/asg_clouth_name*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg_laptop.arn
  }
}

resource  "aws_lb_listener_rule" "digital_rule" {
  listener_arn = aws_lb_listener.home.arn
  priority     = 102

  condition {
    path_pattern {
      values = ["/asg_digital_name*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg_digital.arn
  }
}

resource "aws_autoscaling_attachment" "asg-tg-home" {
  autoscaling_group_name = var.autoscaling_group_name_home
  lb_target_group_arn    = aws_lb_target_group.tg_home.arn
}

resource "aws_autoscaling_attachment" "asg-tg-clouth" {
  autoscaling_group_name = var.autoscaling_group_name_clouth
  lb_target_group_arn    = aws_lb_target_group.tg_clouth.arn
}

resource "aws_autoscaling_attachment" "asg-tg-digital" {
  autoscaling_group_name = var.autoscaling_group_name_digital
  lb_target_group_arn    = aws_lb_target_group.tg_digital.arn
}
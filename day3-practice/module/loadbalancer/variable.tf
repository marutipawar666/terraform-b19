variable "project" {}
variable "env" {}
variable "security_group_id" {}
variable "subnet_ids" {}
variable "autoscaling_group_name_home" {}
variable "autoscaling_group_name_clouth" {}
variable "autoscaling_group_name_digital" {}
variable "vpc_id" {}
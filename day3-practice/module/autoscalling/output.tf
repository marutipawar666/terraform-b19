
output "asg_home_name" {
    value = aws_autoscaling_group.as_home.name
}
output "asg_clouth_name" {
    value = aws_autoscaling_group.as_clouth.name
}
output "asg_digital_name" {
    value = aws_autoscaling_group.as_digital.name
}
